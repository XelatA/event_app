set p / 
'Geburtstag von Karl-Heinz'
'Hochzeit von Susi und Franz'
'Firmenfeier von TUI'
'Test 1'
/

set i / 
'Start Geburtstag'
'Geburtstag planen'
'Raum und Musik'
'Catering, Torte und Florist'
'Vorlaufzeit Geburtstag'
'Diashow'
'Dekoration'
'Ende Geburtstag'
'Start Hochzeit'
'Hochzeit planen'
'Raum Hochzeit'
'Raumdekoration und Sitzordnung'
'Catering, Florist, Life-Band und Fotograf'
'1. Vorlaufzeit Hochzeit'
'Verkostung Hochzeitstorte'
'Hochzeitstorte bestellen'
'2. Vorlaufzeit Hochzeit'
'Ende Hochzeit'
'Start Firmenfeier'
'Firmenfeier planen'
'Raum und Musik Firmenevent'
'Raumdekoration Rednerpult'
'Catering und Florist'
'Vorlaufzeit Firmenfeier'
'Ende Firmenfeier'
'v1'
'v2'
/

set t / 
'1'
'2'
'3'
'4'
'5'
'6'
'7'
'8'
'9'
'10'
'11'
'12'
'13'
'14'
'15'
'16'
'17'
'18'
'19'
'20'
'21'
'22'
'23'
'24'
'25'
'26'
'27'
'28'
'29'
'30'
'31'
'32'
'33'
/;

set pi / 
pi1
pi2
pi3
pi4
pi5
pi6
pi7
pi8
pi9
pi10
pi11
pi12
pi13
pi14
pi15
pi16
pi17
pi18
pi19
pi20
pi21
pi22
pi23
pi24
pi25
pi26
pi27
/;

PIpi(pi,p,i)=no;

PIpi('pi1','Geburtstag von Karl-Heinz','Start Geburtstag')=yes ; 
PIpi('pi2','Geburtstag von Karl-Heinz','Geburtstag planen')=yes ; 
PIpi('pi3','Geburtstag von Karl-Heinz','Raum und Musik')=yes ; 
PIpi('pi4','Geburtstag von Karl-Heinz','Catering, Torte und Florist')=yes ; 
PIpi('pi5','Geburtstag von Karl-Heinz','Vorlaufzeit Geburtstag')=yes ; 
PIpi('pi6','Geburtstag von Karl-Heinz','Diashow')=yes ; 
PIpi('pi7','Geburtstag von Karl-Heinz','Dekoration')=yes ; 
PIpi('pi8','Geburtstag von Karl-Heinz','Ende Geburtstag')=yes ; 
PIpi('pi9','Hochzeit von Susi und Franz','Start Hochzeit')=yes ; 
PIpi('pi10','Hochzeit von Susi und Franz','Hochzeit planen')=yes ; 
PIpi('pi11','Hochzeit von Susi und Franz','Raum Hochzeit')=yes ; 
PIpi('pi12','Hochzeit von Susi und Franz','Raumdekoration und Sitzordnung')=yes ; 
PIpi('pi13','Hochzeit von Susi und Franz','Catering, Florist, Life-Band und Fotograf')=yes ; 
PIpi('pi14','Hochzeit von Susi und Franz','1. Vorlaufzeit Hochzeit')=yes ; 
PIpi('pi15','Hochzeit von Susi und Franz','Verkostung Hochzeitstorte')=yes ; 
PIpi('pi16','Hochzeit von Susi und Franz','Hochzeitstorte bestellen')=yes ; 
PIpi('pi17','Hochzeit von Susi und Franz','2. Vorlaufzeit Hochzeit')=yes ; 
PIpi('pi18','Hochzeit von Susi und Franz','Ende Hochzeit')=yes ; 
PIpi('pi19','Firmenfeier von TUI','Start Firmenfeier')=yes ; 
PIpi('pi20','Firmenfeier von TUI','Firmenfeier planen')=yes ; 
PIpi('pi21','Firmenfeier von TUI','Raum und Musik Firmenevent')=yes ; 
PIpi('pi22','Firmenfeier von TUI','Raumdekoration Rednerpult')=yes ; 
PIpi('pi23','Firmenfeier von TUI','Catering und Florist')=yes ; 
PIpi('pi24','Firmenfeier von TUI','Vorlaufzeit Firmenfeier')=yes ; 
PIpi('pi25','Firmenfeier von TUI','Ende Firmenfeier')=yes ; 
PIpi('pi26','Test 1','v1')=yes ; 
PIpi('pi27','Test 1','v2')=yes ; 



Deadline('Geburtstag von Karl-Heinz')=12/1;
Deadline('Hochzeit von Susi und Franz')=31/1;
Deadline('Firmenfeier von TUI')=19/1;
Deadline('Test 1')=6/1;

PN(p,i)=no;

PN('Geburtstag von Karl-Heinz','Start Geburtstag')=yes;
PN('Geburtstag von Karl-Heinz','Geburtstag planen')=yes;
PN('Geburtstag von Karl-Heinz','Raum und Musik')=yes;
PN('Geburtstag von Karl-Heinz','Catering, Torte und Florist')=yes;
PN('Geburtstag von Karl-Heinz','Vorlaufzeit Geburtstag')=yes;
PN('Geburtstag von Karl-Heinz','Diashow')=yes;
PN('Geburtstag von Karl-Heinz','Dekoration')=yes;
PN('Geburtstag von Karl-Heinz','Ende Geburtstag')=yes;
PN('Hochzeit von Susi und Franz','Start Hochzeit')=yes;
PN('Hochzeit von Susi und Franz','Hochzeit planen')=yes;
PN('Hochzeit von Susi und Franz','Raum Hochzeit')=yes;
PN('Hochzeit von Susi und Franz','Raumdekoration und Sitzordnung')=yes;
PN('Hochzeit von Susi und Franz','Catering, Florist, Life-Band und Fotograf')=yes;
PN('Hochzeit von Susi und Franz','1. Vorlaufzeit Hochzeit')=yes;
PN('Hochzeit von Susi und Franz','Verkostung Hochzeitstorte')=yes;
PN('Hochzeit von Susi und Franz','Hochzeitstorte bestellen')=yes;
PN('Hochzeit von Susi und Franz','2. Vorlaufzeit Hochzeit')=yes;
PN('Hochzeit von Susi und Franz','Ende Hochzeit')=yes;
PN('Firmenfeier von TUI','Start Firmenfeier')=yes;
PN('Firmenfeier von TUI','Firmenfeier planen')=yes;
PN('Firmenfeier von TUI','Raum und Musik Firmenevent')=yes;
PN('Firmenfeier von TUI','Raumdekoration Rednerpult')=yes;
PN('Firmenfeier von TUI','Catering und Florist')=yes;
PN('Firmenfeier von TUI','Vorlaufzeit Firmenfeier')=yes;
PN('Firmenfeier von TUI','Ende Firmenfeier')=yes;
PN('Test 1','v1')=yes;
PN('Test 1','v2')=yes;
VN(p,h,i)=no;

VN('Geburtstag von Karl-Heinz','Start Geburtstag','Geburtstag planen')=yes;
VN('Geburtstag von Karl-Heinz','Geburtstag planen','Raum und Musik')=yes;
VN('Geburtstag von Karl-Heinz','Geburtstag planen','Catering, Torte und Florist')=yes;
VN('Geburtstag von Karl-Heinz','Raum und Musik','Diashow')=yes;
VN('Geburtstag von Karl-Heinz','Raum und Musik','Dekoration')=yes;
VN('Geburtstag von Karl-Heinz','Catering, Torte und Florist','Vorlaufzeit Geburtstag')=yes;
VN('Geburtstag von Karl-Heinz','Vorlaufzeit Geburtstag','Ende Geburtstag')=yes;
VN('Geburtstag von Karl-Heinz','Vorlaufzeit Geburtstag','Ende Geburtstag')=yes;
VN('Hochzeit von Susi und Franz','Start Hochzeit','Hochzeit planen')=yes;
VN('Hochzeit von Susi und Franz','Hochzeit planen','Raum Hochzeit')=yes;
VN('Hochzeit von Susi und Franz','Hochzeit planen','Catering, Florist, Life-Band und Fotograf')=yes;
VN('Hochzeit von Susi und Franz','Hochzeit planen','Verkostung Hochzeitstorte')=yes;
VN('Hochzeit von Susi und Franz','Raum Hochzeit','Raumdekoration und Sitzordnung')=yes;
VN('Hochzeit von Susi und Franz','Catering, Florist, Life-Band und Fotograf','1. Vorlaufzeit Hochzeit')=yes;
VN('Hochzeit von Susi und Franz','Verkostung Hochzeitstorte','Hochzeitstorte bestellen')=yes;
VN('Hochzeit von Susi und Franz','Hochzeitstorte bestellen','2. Vorlaufzeit Hochzeit')=yes;
VN('Hochzeit von Susi und Franz','Raumdekoration und Sitzordnung','Ende Hochzeit')=yes;
VN('Hochzeit von Susi und Franz','1. Vorlaufzeit Hochzeit','Ende Hochzeit')=yes;
VN('Hochzeit von Susi und Franz','2. Vorlaufzeit Hochzeit','Ende Hochzeit')=yes;
VN('Firmenfeier von TUI','Start Firmenfeier','Firmenfeier planen')=yes;
VN('Firmenfeier von TUI','Firmenfeier planen','Raum und Musik Firmenevent')=yes;
VN('Firmenfeier von TUI','Firmenfeier planen','Catering und Florist')=yes;
VN('Firmenfeier von TUI','Raum und Musik Firmenevent','Raumdekoration Rednerpult')=yes;
VN('Firmenfeier von TUI','Catering und Florist','Vorlaufzeit Firmenfeier')=yes;
VN('Firmenfeier von TUI','Raumdekoration Rednerpult','Ende Firmenfeier')=yes;
VN('Firmenfeier von TUI','Vorlaufzeit Firmenfeier','Ende Firmenfeier')=yes;
VN('Test 1','v1','v2')=yes;
d('Start Geburtstag')= 0.0;
k('Start Geburtstag')= 0;

d('Geburtstag planen')= 5.0;
k('Geburtstag planen')= 5;

d('Raum und Musik')= 3.0;
k('Raum und Musik')= 3;

d('Catering, Torte und Florist')= 1.0;
k('Catering, Torte und Florist')= 1;

d('Vorlaufzeit Geburtstag')= 7.0;
k('Vorlaufzeit Geburtstag')= 7;

d('Diashow')= 5.0;
k('Diashow')= 5;

d('Dekoration')= 2.0;
k('Dekoration')= 2;

d('Ende Geburtstag')= 0.0;
k('Ende Geburtstag')= 0;

d('Start Hochzeit')= 0.0;
k('Start Hochzeit')= 0;

d('Hochzeit planen')= 15.0;
k('Hochzeit planen')= 3;

d('Raum Hochzeit')= 4.0;
k('Raum Hochzeit')= 2;

d('Raumdekoration und Sitzordnung')= 5.0;
k('Raumdekoration und Sitzordnung')= 2;

d('Catering, Florist, Life-Band und Fotograf')= 1.0;
k('Catering, Florist, Life-Band und Fotograf')= 1;

d('1. Vorlaufzeit Hochzeit')= 14.0;
k('1. Vorlaufzeit Hochzeit')= 0;

d('Verkostung Hochzeitstorte')= 1.0;
k('Verkostung Hochzeitstorte')= 0;

d('Hochzeitstorte bestellen')= 1.0;
k('Hochzeitstorte bestellen')= 1;

d('2. Vorlaufzeit Hochzeit')= 10.0;
k('2. Vorlaufzeit Hochzeit')= 0;

d('Ende Hochzeit')= 0.0;
k('Ende Hochzeit')= 0;

d('Start Firmenfeier')= 0.0;
k('Start Firmenfeier')= 0;

d('Firmenfeier planen')= 7.0;
k('Firmenfeier planen')= 2;

d('Raum und Musik Firmenevent')= 1.0;
k('Raum und Musik Firmenevent')= 1;

d('Raumdekoration Rednerpult')= 2.0;
k('Raumdekoration Rednerpult')= 1;

d('Catering und Florist')= 1.0;
k('Catering und Florist')= 1;

d('Vorlaufzeit Firmenfeier')= 7.0;
k('Vorlaufzeit Firmenfeier')= 0;

d('Ende Firmenfeier')= 0.0;
k('Ende Firmenfeier')= 0;

d('v1')= 2.0;
k('v1')= 3;

d('v2')= 2.0;
k('v2')= 3;

KP=4;
oc=160;
