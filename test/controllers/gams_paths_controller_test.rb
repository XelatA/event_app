require 'test_helper'

class GamsPathsControllerTest < ActionController::TestCase
  setup do
    @gams_path = gams_paths(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:gams_paths)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create gams_path" do
    assert_difference('GamsPath.count') do
      post :create, gams_path: { gams_path_url: @gams_path.gams_path_url }
    end

    assert_redirected_to gams_path_path(assigns(:gams_path))
  end

  test "should show gams_path" do
    get :show, id: @gams_path
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @gams_path
    assert_response :success
  end

  test "should update gams_path" do
    patch :update, id: @gams_path, gams_path: { gams_path_url: @gams_path.gams_path_url }
    assert_redirected_to gams_path_path(assigns(:gams_path))
  end

  test "should destroy gams_path" do
    assert_difference('GamsPath.count', -1) do
      delete :destroy, id: @gams_path
    end

    assert_redirected_to gams_paths_path
  end
end
