User.create!(name:  "Example User",
             email: "example@railstutorial.org",
             password:              "foobar",
             password_confirmation: "foobar",
             admin: true)

User.create!(name:  "Xelat Akcam",
             email: "xelat258@gmail.com",
             password:              "foobar",
             password_confirmation: "foobar",
             admin: true)


99.times do |n|
  name  = Faker::Name.name
  email = "example-#{n+1}@railstutorial.org"
  password = "password"
  User.create!(name:  name,
               email: email,
               password:              password,
               password_confirmation: password)
end

users = User.order(:created_at).take(6)
50.times do
  content = Faker::Lorem.sentence(5)
  users.each { |user| user.microposts.create!(content: content) }
end


GamsPath.create!(gams_path_url: "C:\\GAMS\\win64\\24.7\\gams")
## Events
A = Event.create!(name: "Geburtstag von Karl-Heinz",   guest: 70, deadline: Time.now+13.days, eventtext: "Ich wünsche zu meinem 50. Geburtstag ein Burger-Buffet und eine Torte mit Eierlikör.")
B = Event.create!(name: "Hochzeit von Susi und Franz", guest: 200,deadline: Time.now+32.days, eventtext: "Zu unserem besonderen Tag wünschen Franz und ich uns ein Schloss als Location und Lavendelfarbene Dekoration.")
C = Event.create!(name: "Firmenfeier von TUI",         guest: 300,deadline: Time.now+20.days, eventtext: "Zur diesjährigen Betriebsfeier wünschen wir uns eine Jazz-Band und ein Rednerpult.")

### Perioden
(0..32).each do |n|
  name = "t#{n}"
  time=Time.now+n.days
  Period.create!(name: name, time: time)
end


### Vorgänge
AA1=Job.create!(event_id:1, name: "Start Geburtstag",    processing_time: 0, ressource_demand:0, jobtext: "Dummy-Geburtstag von Karl-Heinz." )
AA2=Job.create!(event_id:1, name: "Geburtstag planen",          processing_time: 5, ressource_demand:5, jobtext: "Individuelle Wünsche des Geburtstagskindes und seiner Familie aufnehmen und bei der weiteren Planung berücksichtigen.")
AA3=Job.create!(event_id:1, name: "Raum und Musik",   processing_time: 3, ressource_demand:3, jobtext: "Passenden Raum im Landgut-Stil nach Wunsch der Familie und zur Anzahl der Personen auswählen und buchen, Raum vorher besichtigen. Life-Band, die sowohl Volksmusik als auch Charts spielt, auswählen und buchen." )
AA4=Job.create!(event_id:1, name: "Catering, Torte und Florist",   processing_time: 1, ressource_demand:1, jobtext: "Burger-Buffet zur Selbstbedienung bestellen: verschiedene Brotsorten, verschiedene Soßen, verschiedene Salat-Beilagen, Fleisch: Rinderhacksteaks und Pulled Pork. Geburtstagstorte mit Lieblingsgetränk von Karl-Heinz, Eierlikör, und der Aufschrift „Alles Gute zum 50.ten Geburtstag, Karl-Heinz“ beim Konditor in Auftrag geben. Tulpen in den Farben gelb und orange zur Dekoration des Raumes bestellen.")
AA5=Job.create!(event_id:1, name: "Vorlaufzeit Geburtstag",                processing_time: 7, ressource_demand:7, jobtext: "Berücksichtigung Vorlaufzeit Catering, Konditor und Florist.")
AA6=Job.create!(event_id:1, name: "Diashow",                    processing_time: 5, ressource_demand:5, jobtext: "Diashow zum Thema '50 Jahre aus dem Leben von Karl-Heinz'.")
AA7=Job.create!(event_id:1, name: "Dekoration",                 processing_time: 2, ressource_demand:2, jobtext: "Servietten, Luftballons und Girlanden im passenden Motiv '50' auswählen und einkaufen.")
AA8=Job.create!(event_id:1, name: "Ende Geburtstag",            processing_time: 0, ressource_demand:0, jobtext: "Dummy-Geburtstag kann statt finden.")


BB1=Job.create!(event_id:2, name: "Start Hochzeit",                         processing_time: 0,  ressource_demand:0, jobtext:"Dummy-Hochzeit von Susi und Franz.")
BB2=Job.create!(event_id:2, name: "Hochzeit planen",                        processing_time: 15, ressource_demand:3, jobtext:"Individuelle Wünsche des Geburtstagskindes und seiner Familie aufnehmen und bei der weiteren Planung berücksichtigen.")
BB3=Job.create!(event_id:2, name: "Raum Hochzeit",                                   processing_time: 4,  ressource_demand:2, jobtext:"Brautpaar wünscht sich einen Schlosssaal.")
BB4=Job.create!(event_id:2, name: "Raumdekoration und Sitzordnung",            processing_time: 5,  ressource_demand:2, jobtext:"Servietten, Luftballons und Perlen passend zur Blumendekoration in den Farben weiß und lavendel auswählen und einkaufen. Anhand der Gästeliste Plätze zuweisen und Platzkarten drucken.")
BB5=Job.create!(event_id:2, name: "Catering, Florist, Life-Band und Fotograf", processing_time: 1,  ressource_demand:1, jobtext:"3. Gänge-Menü bestellen: 1. Hochzeitssuppe, 2. Ente, Klöße und Rotkohl (alternativ für Vegetarier: Lachs, Reis und Spinat), 3. Mousse au Chocolat und Crème Brûlée. Blumen: Lavendel und weiße Rosen zur Dekoration des Raumes bestellen. Band, die sowohl ruhige als auch lebhafte Musik spielt. Einen professionellen Hochzeitsfotografen buchen.")
BB6=Job.create!(event_id:2, name: "1. Vorlaufzeit Hochzeit",                            processing_time: 14, ressource_demand:0, jobtext:"Berücksichtigung Vorlaufzeit Catering und Florist.")
BB7=Job.create!(event_id:2, name: "Verkostung Hochzeitstorte",              processing_time: 1,  ressource_demand:0, jobtext:"Verkostung der Torte in Schokoladen-, Erdbeer-, Himbeer-, und Krokantgeschmack.")
BB8=Job.create!(event_id:2, name: "Hochzeitstorte bestellen",               processing_time: 1,  ressource_demand:1, jobtext:"5-stöckige Hochzeitstorte beim Konditor in Auftrag geben.")
BB9=Job.create!(event_id:2, name: "2. Vorlaufzeit Hochzeit",                            processing_time: 10, ressource_demand:0, jobtext:"Berücksichtigung Vorlaufzeit Konditor")
BB10=Job.create!(event_id:2, name: "Ende Hochzeit",                         processing_time: 0,  ressource_demand:0, jobtext:"Dummy-Hochzeit kann statt finden.")

CC1=Job.create!(event_id:3, name:"Start Firmenfeier",           processing_time: 0, ressource_demand:0, jobtext:"Dummy-Firmenfeier TUI")
CC2=Job.create!(event_id:3, name:"Firmenfeier planen",          processing_time: 7, ressource_demand:2, jobtext:"Alljährliche Feier zur Präsentation der TUI in Zahlen und zur Ehrung einiger Mitarbeiter.")
CC3=Job.create!(event_id:3, name:"Raum und Musik Firmenevent",    processing_time: 1, ressource_demand:1, jobtext:"Modernen Raum, Jazz-Band beauftragen.")
CC4=Job.create!(event_id:3, name:"Raumdekoration Rednerpult",  processing_time: 2, ressource_demand:1, jobtext:"Tischdekoration auswählen und einkaufen. Rednerpult für Reden der Geschäftsführer zur Performance des Geschäftsjahres sowie zur Hervorhebung besonderer Leistungen einzelner Mitarbeiter")
CC5=Job.create!(event_id:3, name:"Catering und Florist",           processing_time: 1, ressource_demand:1, jobtext:"Passend zur Tischdeko.")
CC6=Job.create!(event_id:3, name:"Vorlaufzeit Firmenfeier",                 processing_time: 7, ressource_demand:0, jobtext:"Berücksichtigung Vorlaufzeit Catering und Florist")
CC7=Job.create!(event_id:3, name:"Ende Firmenfeier",            processing_time: 0, ressource_demand:0, jobtext:"Dummy-Firmenfeier kann statt finden.")


# Vorgänger-Nachfolger-Beziehung für Event "Geburtstag"

AAA1=EventJobJobAssociation.create!(event_id:1, predecessor_id: 1, successor_id:2)
AAA2=EventJobJobAssociation.create!(event_id:1, predecessor_id: 2, successor_id:3)
AAA3=EventJobJobAssociation.create!(event_id:1, predecessor_id: 2, successor_id:4)
AAA4=EventJobJobAssociation.create!(event_id:1, predecessor_id: 3, successor_id:6)
AAA5=EventJobJobAssociation.create!(event_id:1, predecessor_id: 3, successor_id:7)
AAA6=EventJobJobAssociation.create!(event_id:1, predecessor_id: 4, successor_id:5)
AAA7=EventJobJobAssociation.create!(event_id:1, predecessor_id: 5, successor_id:8)
AAA8=EventJobJobAssociation.create!(event_id:1, predecessor_id: 5, successor_id:8)

# Vorgänger-Nachfolger-Beziehung für Event "Hochzeit"
BBB1=EventJobJobAssociation.create!(event_id:2, predecessor_id:9, successor_id: 10)
BBB2=EventJobJobAssociation.create!(event_id:2, predecessor_id:10, successor_id: 11)
BBB3=EventJobJobAssociation.create!(event_id:2, predecessor_id:10, successor_id: 13)
BBB4=EventJobJobAssociation.create!(event_id:2, predecessor_id:10, successor_id: 15)
BBB5=EventJobJobAssociation.create!(event_id:2, predecessor_id:11, successor_id: 12)
BBB6=EventJobJobAssociation.create!(event_id:2, predecessor_id:13, successor_id: 14)
BBB7=EventJobJobAssociation.create!(event_id:2, predecessor_id:15, successor_id: 16)
BBB8=EventJobJobAssociation.create!(event_id:2, predecessor_id:16, successor_id: 17)
BBB9=EventJobJobAssociation.create!(event_id:2, predecessor_id:12, successor_id: 18)
BBB10=EventJobJobAssociation.create!(event_id:2, predecessor_id:14, successor_id: 18)
BBB11=EventJobJobAssociation.create!(event_id:2, predecessor_id:17, successor_id: 18)

# Vorgänger-Nachfolger-Beziehung für Event "Firmenfeier"
CCC1=EventJobJobAssociation.create!(event_id:3, predecessor_id:19, successor_id: 20)
CCC2=EventJobJobAssociation.create!(event_id:3, predecessor_id:20, successor_id: 21)
CCC3=EventJobJobAssociation.create!(event_id:3, predecessor_id:20, successor_id: 23)
CCC4=EventJobJobAssociation.create!(event_id:3, predecessor_id:21, successor_id: 22)
CCC5=EventJobJobAssociation.create!(event_id:3, predecessor_id:23, successor_id: 24)
CCC6=EventJobJobAssociation.create!(event_id:3, predecessor_id:22, successor_id: 25)
CCC7=EventJobJobAssociation.create!(event_id:3, predecessor_id:24, successor_id: 25)


# Event-Vorgang-Perioden Association

PIT1=EventJobPeriodAssociation.create!(event_id:1, job_id:1)

PIT2=EventJobPeriodAssociation.create!(event_id:1, job_id:2)

PIT3=EventJobPeriodAssociation.create!(event_id:1, job_id:3)
PIT4=EventJobPeriodAssociation.create!(event_id:1, job_id:4)

PIT5=EventJobPeriodAssociation.create!(event_id:1, job_id:5)

PIT6=EventJobPeriodAssociation.create!(event_id:1, job_id:6)

PIT7=EventJobPeriodAssociation.create!(event_id:1, job_id:7)

PIT8=EventJobPeriodAssociation.create!(event_id:1, job_id:8)

PIT9=EventJobPeriodAssociation.create!(event_id:2, job_id:9)

PIT10=EventJobPeriodAssociation.create!(event_id:2, job_id:10)

PIT11=EventJobPeriodAssociation.create!(event_id:2, job_id:11)

PIT12=EventJobPeriodAssociation.create!(event_id:2, job_id:12)
PIT13=EventJobPeriodAssociation.create!(event_id:2, job_id:13)

PIT14=EventJobPeriodAssociation.create!(event_id:2, job_id:14)
PIT15=EventJobPeriodAssociation.create!(event_id:2, job_id:15)

PIT16=EventJobPeriodAssociation.create!(event_id:2, job_id:16)
PIT17=EventJobPeriodAssociation.create!(event_id:2, job_id:17)

PIT18=EventJobPeriodAssociation.create!(event_id:2, job_id:18)
PIT19=EventJobPeriodAssociation.create!(event_id:3, job_id:19)

PIT20=EventJobPeriodAssociation.create!(event_id:3, job_id:20)

PIT21=EventJobPeriodAssociation.create!(event_id:3, job_id:21)

PIT22=EventJobPeriodAssociation.create!(event_id:3, job_id:22)
PIT23=EventJobPeriodAssociation.create!(event_id:3, job_id:23)

PIT24=EventJobPeriodAssociation.create!(event_id:3, job_id:24)
PIT25=EventJobPeriodAssociation.create!(event_id:3, job_id:25)
