class CreatePeriods < ActiveRecord::Migration
  def change
    create_table :periods do |t|
      t.date :time
      t.integer :add_cappa

      t.timestamps null: false
    end
  end
end
