class CreateUsers < ActiveRecord::Migration
  def change
    if (!ActiveRecord::Base.connection.tables.include?("users"))
    create_table :users do |t|
      t.string :name
      t.string :email

      t.timestamps null: false
    end
  end
  end
  end
