json.array!(@periods) do |period|
  json.extract! period, :id, :time, :add_cappa
  json.url period_url(period, format: :json)
end
