class Job < ActiveRecord::Base
  belongs_to :event
  has_many :event_job_job_associations, foreign_key: "successor_id", :dependent => :destroy
  has_many :event_reverse_job_associations, foreign_key: "predecessor_id", class_name: "EventJobJobAssociation", :dependent => :destroy
  has_many :successors, through: :event_job_job_associations, source: :successor_id
  has_many :predecessors, through: :event_reverse_job_associations, source: :predecessor_id
end
