* Ressourcenbeschraenkte Projektplanung in diskreter Zeit
* Zwei Modellvarianten:
* Variante 1: Minimierung der Projektdauer bei gegebenen Kapazitaeten
* Variante 2: Minimierung der Kosten fuer Zusatzkapazitaet bei
*             gegebener Deadline

set
    p Event
    i Vorgang
    t Periode
;
alias(t,tau);
alias(h,i);

set
    pi
    PIpi(pi,p,i)
    
    ;


set
    PN(p,i) Event-Vorgang-Relation zwischen p und i;
set
    VN(p,h,i) Vorgaenger-Nachfolger-Relation zwischen h und i;

parameter
    d(i)    Dauer
    FA(p,i)   Fruehester Anfangszeitpunkt
    FE(p,i)   Fruehester Endzeitpunkt
    SA(p,i)   Spaetester Anfangszeitpunkt
    SE(p,i)   Spaetester Endzeitpunkt
    k(i)  Kapazitaetsbedarf von Vorgang i auf Ressource r
    KP   Kapazitaet je Periode von Ressource r
    oc   Kosten einer Einheit Zusatzkapazitaet
    ihilf
    philf
    Deadline(p)
    MinimaleDauer(p)   ;

binary variables
    x(p,i,t)  gleich 1 wenn Vorgang i von Event p in Periode t beendet wird;

free variables
    Z       Zielfunktionswert;

positive variables
    O(t)  Zusatzkapazitaet in Periode t;


* Daten der Instanz
$include Multi_Projekt_Daten.inc

* Zeitrechnung
* Achtung: Topologische Sortierung wird unterstellt

MinimaleDauer(p)=0;
FA(p,i)$PN(p,i)=0;
FE(p,i)$PN(p,i)=d(i);

loop((p,i)$PN(p,i),
     loop(h$VN(p,h,i),
         if(FE(p,h)>FA(p,i),
             FA(p,i)=FE(p,h);
             FE(p,i)=FA(p,i)+d(i);
             if( FE(p,i)>MinimaleDauer(p),
                 MinimaleDauer(p) = FE(p,i)
             );
         );
     );
);



SE(p,i)$PN(p,i)=max(MinimaleDauer(p), Deadline(p));
SA(p,i)$PN(p,i)=SE(p,i)$PN(p,i)-d(i);

for(ihilf=card(i) downto 1,
     loop((p,i)$((ord(i)=round(ihilf)) and PN(p,i)),
         loop(h$VN(p,i,h),
             if(SA(p,h)<SE(p,i),
                 SE(p,i)=SA(p,h);
                 SA(p,i)=SE(p,i)-d(i);
             );
         );
     );
);





display d, FA, FE, SA, SE, Deadline, MinimaleDauer;


Equations
    ZielfunktionKosten
    JederVorgangEinmal(p,i)
    Projektstruktur(p,h,i)
    Kapazitaetsrestriktionalt(t)
    KapazitaetsrestriktionFlexalt(t)
    KapazitaetsrestriktionFlex(t);


ZielfunktionKosten..
    Z=e=sum(t,oc*O(t));

JederVorgangEinmal(p,i)$PN(p,i)..
    sum(t$(FE(p,i)<=ord(t)-1 and ord(t)-1 <= SE(p,i)), x(p,i,t)) =e= 1;

Projektstruktur(p,h,i)$(VN(p,h,i) and PN(p,i))..
    sum(t$(FE(p,h)<=ord(t)-1 and ord(t)-1 <= SE(p,h)),
          (ord(t)-1)*x(p,h,t))  =l=
    sum(t$(FE(p,i)<=ord(t)-1 and ord(t)-1 <= SE(p,i)),
          (ord(t)-1-d(i))*x(p,i,t));


KapazitaetsrestriktionFlex(t)..
   sum((p,i)$PN(p,i),
    sum(tau$((ord(tau)-1 >= max(ord(t)-1, FE(p,i)))  and
              (ord(tau)-1 <= min(ord(t)-1+d(i)-1, SE(p,i)))),
          k(i)*x(p,i,tau)))=l=KP+O(t);

$ontext
model RCPSP1 /
    ZielfunktionZeit
    JederVorgangEinmal
    Projektstruktur
    Kapazitaetsrestriktion/;
$offtext

model RCPSP1 /
    ZielfunktionKosten
    JederVorgangEinmal
    Projektstruktur
    KapazitaetsrestriktionFlex/;


RCPSP1.optcr=0.0;
RCPSP1.limrow=500;

solve RCPSP1 minimizing Z using mip;
 display x.l, O.l;


**Start- und Endzeitpunkte

file outputfile1 / 'TCPSP_solution_PI.txt'/;
put outputfile1;
loop(p,
loop(i$PN(p,i),
             put p.tl:0, ' ; 'i.tl:0, '; ', FA(p,i),' ; ', FE(p,i), ' ; ', SA(p,i) , ' ; ', SE(p,i) /
         );
     );


putclose outputfile1;


**Ende von Vorg�ngen


file outputfile2 / 'TCPSP_solution_PIT.txt'/;
put outputfile2;


loop(p,
     loop(i,
         loop(t,
         loop((pi)$PIpi(pi,p,i),
if (x.l(p,i,t)=1,
             put pi.tl:0, ' ; ' p.tl:0, ' ; ' i.tl:0, ' ; ', t.tl:0, ' ; ', x.l(p,i,t) /
);
         );
     );
);
);

putclose outputfile2;


**Zusatzkapazit�t

file outputfile3 / 'TCPSP_solution_add_cappa.txt'/;
put outputfile3;


loop(t,
             put t.tl:0, ' ;', O.l(t) /
);


putclose outputfile3;

**Zielfunktion

file outputfile4 / 'TCPSP_Zfkt.txt'/;
put outputfile4;


put 'Zielfunktionswert:  ',Z.l /
put '**************************'

putclose outputfile4;



